// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSC.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDSC, "TDSC" );

DEFINE_LOG_CATEGORY(LogTDSC)
 