// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCGameMode.h"
#include "TDSCPlayerController.h"
#include "TDSCCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDSCGameMode::ATDSCGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDSCPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	//Blueprint'/Game/Blueprint/Character/TopDownCharacter.TopDownCharacter'
}